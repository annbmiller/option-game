package game.api.market

import game.CandlesNotFoundException
import game.api.models.{ListCandles, RequestCandles}
import game.api.OpenApiInterface

import org.joda.time.DateTime
import slogging.StrictLogging
import scala.concurrent.{ExecutionContext, Future}

class Candles(api: OpenApiInterface)(implicit executionContext: ExecutionContext) extends StrictLogging {

  /**
   * Получение минутных свечей по figi за последние 30 мин к date
   * @param figi
   * @param date
   * @return
   */
    def getForTime(figi: String, date: Long): Future[ListCandles] = {
      val to   = new DateTime(date).withMillisOfSecond(0).withSecondOfMinute(0)
      val from = to.minusMinutes(30)

      api.getCandles(RequestCandles(figi, from.toString, to.toString, "1min")).flatMap {
        case Some(list) => Future.successful(list.copy(candles = list.candles.reverse))
        case _ => Future.failed(CandlesNotFoundException(figi))
      }
    }
}
