package game.api.market

import akka.actor.ActorSystem
import slogging.StrictLogging
import game.api.OpenApiInterface
import game.api.models.{Instrument, ListInstruments}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Random, Success}

class Stocks(api: OpenApiInterface)(implicit system: ActorSystem, ec: ExecutionContext) extends StrictLogging {

  system.scheduler.scheduleWithFixedDelay(0.seconds, 10.minutes) { () => requestForStocks }

  val topCount = 10

  /**
   * Фильтрация для выборки
   */
  val filterForTop: Instrument => Boolean = _.minPriceIncrement match {
    case Some(minPriceInc: BigDecimal) => minPriceInc.compare(0.01) <= 0 && minPriceInc.compare(0.001) > 0
    case _ => false
  }

  private var listInstuments: ListInstruments = ListInstruments(0, Seq())

  /**
   * Запрос к апи
   */
  private def requestForStocks: Future[ListInstruments] = api.getStocks.andThen {
    case Success(stocks)    => listInstuments = stocks
    case Failure(exception) => logger.error(exception.getMessage)
  }

  /**
   * Получение списка инструментов из апи или из кэша
   */
  private def getAllStocks: Future[ListInstruments] = if (listInstuments.instruments.isEmpty) {
    requestForStocks
  } else Future.successful(listInstuments)

  /**
   * 10 случайных ликвидных инструментов
   */
  def getTop: Future[Seq[Instrument]] = getAllStocks.map(list => {
    val instruments = list.instruments.filter(filterForTop)
    Random.shuffle(instruments).take(topCount)
  })

  /**
   * Поиск по figi
   * TODO: сделать поиск по Map вместо Seq
   */
  def find(figi: String): Future[Option[Instrument]] = getAllStocks.map( list =>
    list.instruments.find(_.figi == figi)
  )

}
