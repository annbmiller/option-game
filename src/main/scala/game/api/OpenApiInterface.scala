package game.api

import game.api.models.{ListCandles, ListInstruments, RequestCandles}

import scala.concurrent.Future

abstract class OpenApiInterface {
  def getStocks: Future[ListInstruments]
  def getCandles(params: RequestCandles): Future[Option[ListCandles]]
}
