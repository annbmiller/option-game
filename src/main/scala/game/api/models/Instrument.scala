package game.api.models

import io.circe.{Decoder, Encoder}, io.circe.generic.semiauto._

case class Instrument(figi: String,
                      ticker: String,
                      isin: String,
                      minPriceIncrement: Option[BigDecimal],
                      lot: Int,
                      currency: String,
                      name: String,
                      `type`: String)

object Instrument {
  implicit val jsonDecoder: Decoder[Instrument] = deriveDecoder
  implicit val jsonEncoder: Encoder[Instrument] = deriveEncoder
}