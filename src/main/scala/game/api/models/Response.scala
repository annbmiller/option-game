package game.api.models

import cats.syntax.functor._
import io.circe.{Decoder, Encoder, Json}
import io.circe.generic.auto._
import io.circe.syntax._

sealed trait Response

case class ResponseOk(trackingId: String, status: String, payload: Json) extends Response

case class ResponseError(trackingId: String, status: String, payload: MessageError) extends Response

object Response {
  implicit val jsonDecoder: Decoder[Response] = List[Decoder[Response]](
    Decoder[ResponseError].widen,
    Decoder[ResponseOk].widen,
  ).reduceLeft(_ or _)

  implicit val jsonEncoder: Encoder[Response] = Encoder.instance {
    case error@ResponseError(_, _, _) => error.asJson
    case ok@ResponseOk(_, _, _) => ok.asJson
  }
}


