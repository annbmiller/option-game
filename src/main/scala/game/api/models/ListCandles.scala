package game.api.models

import io.circe.{Decoder, Encoder}, io.circe.generic.semiauto._

case class ListCandles(candles: Seq[Candle],
                       interval: String,
                       figi: String)

object ListCandles {
  implicit val jsonDecoder: Decoder[ListCandles] = deriveDecoder
  implicit val jsonEncoder: Encoder[ListCandles] = deriveEncoder
}

