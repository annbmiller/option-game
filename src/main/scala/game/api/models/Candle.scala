package game.api.models

import io.circe.{Decoder, Encoder}, io.circe.generic.semiauto._

case class Candle(figi: String,
                  interval: String,
                  o: Double,
                  c: Double,
                  h: Double,
                  l: Double,
                  v: Double,
                  time: String)

object Candle {
  implicit val jsonDecoder: Decoder[Candle] = deriveDecoder
  implicit val jsonEncoder: Encoder[Candle] = deriveEncoder
}

