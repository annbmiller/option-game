package game.api.models

import akka.http.scaladsl.model.Uri.Query
import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

case class RequestCandles(figi: String,
                          from: String,
                          to: String,
                          interval: String) {
  def toQuery: Query =
    Query(Map("figi" -> figi, "from" -> from, "to" -> to, "interval" -> interval))

  def toQueryString: String = toQuery.toString()
}

object RequestCandles {
  implicit val jsonDecoder: Decoder[RequestCandles] = deriveDecoder
  implicit val jsonEncoder: Encoder[RequestCandles] = deriveEncoder
}



