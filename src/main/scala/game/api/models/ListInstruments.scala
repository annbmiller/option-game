package game.api.models

import io.circe.{Decoder, Encoder}, io.circe.generic.semiauto._

case class ListInstruments(total: Int,
                          instruments: Seq[Instrument])

object ListInstruments {
  implicit val jsonDecoder: Decoder[ListInstruments] = deriveDecoder
  implicit val jsonEncoder: Encoder[ListInstruments] = deriveEncoder
}