package game.api.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

case class MessageError(message: String, code: String)

object MessageError {
  implicit val jsonDecoder: Decoder[MessageError] = deriveDecoder
  implicit val jsonEncoder: Encoder[MessageError] = deriveEncoder
}

