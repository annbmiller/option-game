package game.api

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal

import scala.concurrent.{ExecutionContext, Future}

object HttpClient {
  def dispatchRequest(request: HttpRequest)(implicit system: ActorSystem, ec: ExecutionContext): Future[String] =
    Http().singleRequest(request).flatMap(Unmarshal(_).to[String])
}
