package game.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Authorization
import slogging.StrictLogging
import io.circe.jawn.decode
import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

import game.api.models.{MessageError, Response, ResponseError}

class Request(uri: String, params: String)
             (implicit val authorization: Authorization, ec: ExecutionContext, as: ActorSystem) extends StrictLogging {

  private def prepareRequest(method: HttpMethod) = HttpRequest(
    method = method,
    uri = uri,
    entity = HttpEntity(ContentTypes.`application/json`, params),
    headers = Seq(authorization)
  )

  private def getResponse(response: String): Response = decode[Response](response) match {
      case Right(resp) => resp
      case Left(error) =>
        logger.error(error.getMessage)
        ResponseError("", "", MessageError(error.getMessage, ""))
  }

  def post: Future[Response] = HttpClient.dispatchRequest(prepareRequest(HttpMethods.POST)).map(getResponse)

  def get: Future[Response] = HttpClient.dispatchRequest(prepareRequest(HttpMethods.GET)).map(getResponse)

}

