package game.api

import akka.actor.ActorSystem

import scala.concurrent.ExecutionContext

class OpenApiSandbox(token: String)
                    (implicit executionContext: ExecutionContext, as: ActorSystem) extends OpenApi(token) {
  override val uri = "https://api-invest.tinkoff.ru/openapi/sandbox/"
}
