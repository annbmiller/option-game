package game.api

import akka.actor.ActorSystem
import akka.http.javadsl.model.headers.HttpCredentials
import akka.http.scaladsl.model.headers.Authorization
import game.api.models.{ListCandles, ListInstruments, RequestCandles, ResponseError, ResponseOk}
import slogging.StrictLogging

import scala.concurrent.{ExecutionContext, Future}

class OpenApi(token: String)
             (implicit val executionContext: ExecutionContext, as: ActorSystem) extends OpenApiInterface
  with StrictLogging {

  private[api] val uri: String              = "https://api-invest.tinkoff.ru/openapi/"
  implicit val authorization: Authorization = Authorization(HttpCredentials.createOAuth2BearerToken(token))

  def getStocks: Future[ListInstruments] =
    new Request(uri + "market/stocks", "").get
      .map {
        case responseOk: ResponseOk => responseOk.payload.as[ListInstruments].getOrElse(ListInstruments(0, Seq()))
        case errorResponse: ResponseError =>
          logger.error(errorResponse.payload.message)
          ListInstruments(0, Seq())
      }

      .recover {
        case ex =>
          logger.error(ex.getMessage)
          ListInstruments(0, Seq())
      }

  def getCandles(params: RequestCandles): Future[Option[ListCandles]] =
    new Request(uri + "market/candles?" + params.toQueryString, "").get
      .map {
        case responseOk: ResponseOk => responseOk.payload.as[ListCandles].toOption
        case errorResponse: ResponseError =>
          logger.error(errorResponse.payload.message)
          None
      }
}
