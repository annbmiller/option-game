package game.db

import slick.jdbc.JdbcBackend.{Database => slickDB}

class Database {
  val db = slickDB.forConfig("h2mem1")
}
