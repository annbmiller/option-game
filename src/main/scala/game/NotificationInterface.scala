package game

import game.modules.bet.models.Bet

import scala.concurrent.Future

trait NotificationInterface {
  def sendErrorTimingMessage: Long => Future[Unit]

  def sendEndOfCalcMessage: Bet => Future[Unit]

  def sendErrorCalculationMessage: (Long, String) => Future[Unit]
}
