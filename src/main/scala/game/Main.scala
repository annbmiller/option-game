package game

import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import game.bot.GameBot
import game.modules.app.AppModule

import scala.concurrent.{Await, ExecutionContextExecutor}
import scala.concurrent.duration.Duration

object Main extends App {

  val cfg = ConfigFactory.load()

  implicit val system: ActorSystem = ActorSystem("app", cfg)
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val appModule = new AppModule(cfg.getString("api.token"))
  val bot       = new GameBot(cfg.getString("telegram.token"), appModule)
  val daemon    = new Daemon(appModule, bot)

  val program   = bot.run()
  daemon.runDaemon()

  println("Press [ENTER] to shutdown the bot, it may take a few seconds...")
  scala.io.StdIn.readLine()

  daemon.shutdownDaemon()
  bot.shutdown()
  Await.result(program, Duration.Inf)

}
