package game.modules.user

case class User(id: Long, name: String, score: Int)
