package game.modules.user

import game.db.Database

import scala.concurrent.ExecutionContext

class UserModule(db: Database)(implicit ec: ExecutionContext) extends UserModuleInterface {
  private val userRepository = new UserRepository(db.db)
  val userService            = new UserService(userRepository)
}
