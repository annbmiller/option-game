package game.modules.user

import com.bot4s.telegram.models
import game.UserNotFoundException

import scala.concurrent.{ExecutionContext, Future}

class UserService(userRepository: UserRepository)(implicit executionContext: ExecutionContext) {
  private def add(user: models.Chat): Future[User] = userRepository.add(
    User(user.id, user.firstName.getOrElse("") + " " + user.lastName.getOrElse(""), 100)
  )

  def find(id: Long): Future[Option[User]] = userRepository.find(id)

  def findAndSetScore(id: Long, newScore: Int => Int): Future[User] = find(id).flatMap{
    case Some(user) =>userRepository.update(user.copy(score = newScore(user.score)))
    case _ => Future.failed(UserNotFoundException(id))
  }


  def findOrAdd(user: models.Chat): Future[User] = userRepository.find(user.id).flatMap {
    case Some(user) => Future.successful(user)
    case _ => add(user)
  }

  def getAll: Future[Seq[User]] = userRepository.getAll

  def getBalance(id: Long): Future[Option[Int]] = userRepository.getBalance(id)

  def getTop: Future[Seq[User]] = userRepository.getTopNByScore(10)
}
