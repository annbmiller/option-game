package game.modules.user

import slick.dbio.{DBIOAction, Effect}
import slick.jdbc.H2Profile.api._
import slick.lifted.ProvenShape
import UserTableRepository.AllUsers

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

private[user] class UserRepository(db: Database)(implicit ec: ExecutionContext) {
  Await.result(db.run(AllUsers.schema.createIfNotExists), Duration.Inf)

  def add(user: User): Future[User] = db.run(UserTableRepository.add(user)).map(_ => user)

  def find(id: Long): Future[Option[User]] = db.run(UserTableRepository.find(id))

  def getAll: Future[Seq[User]] = db.run(UserTableRepository.getAll)

  def update(user: User): Future[User] = db.run(UserTableRepository.update(user)).map(_ => user)

  def getBalance(id: Long): Future[Option[Int]] = db.run(UserTableRepository.getBalance(id))

  def getTopNByScore(n: Int): Future[Seq[User]] = db.run(UserTableRepository.getTopNByScore(n))
}

class UsersTable(tag: Tag) extends Table[User](tag, "USERS") {
  def id: Rep[Long] = column("id", O.PrimaryKey)

  def name: Rep[String] = column("name")

  def score: Rep[Int] = column("score")

  override def * : ProvenShape[User] = (id, name, score).mapTo[User]
}

object UserTableRepository {
  val AllUsers = TableQuery[UsersTable]

  def add(user: User): DBIOAction[Int, NoStream, Effect.Write] = AllUsers += user

  def find(id: Long): DBIOAction[Option[User], NoStream, Effect.Read] = AllUsers.filter(_.id === id).result.headOption

  def getAll: DBIOAction[Seq[User], NoStream, Effect.Read] = AllUsers.result

  def getBalance(id: Long): DBIOAction[Option[Int], NoStream, Effect.Read] = AllUsers.filter(_.id === id).map(_.score).result.headOption

  def getTopNByScore(n: Int): DBIOAction[Seq[User], NoStream, Effect.Read] = AllUsers.sortBy(_.score.desc).take(n).result

  def update(user: User): DBIOAction[Int, NoStream, Effect.Write] = AllUsers.filter(_.id === user.id).update(user)
}
