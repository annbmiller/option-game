package game.modules.app

import akka.actor.ActorSystem
import scala.concurrent.ExecutionContext

import game.api.OpenApi
import game.api.market.{Candles, Stocks}
import game.db.Database
import game.modules.bet.BetModule
import game.modules.user.UserModule

class AppModule(apiToken: String) (implicit system: ActorSystem, ec: ExecutionContext) {
  private val db = new Database
  val userModule: UserModule = new UserModule(db)
  val betModule: BetModule = new BetModule(db)

  private val api = new OpenApi(apiToken)
  val stocks = new Stocks(api)
  val candles = new Candles(api)
}
