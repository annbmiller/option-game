package game.modules.bet.models

import enumeratum.{CirceEnum, Enum, EnumEntry}
import scala.collection.immutable.IndexedSeq

sealed trait BetStatus extends EnumEntry

object BetStatus extends Enum[BetStatus] with CirceEnum[BetStatus] {

  case object BetStatusInProgress extends BetStatus {
    override def toString: String = "В процессе создания"
  }
  case object BetStatusWait extends BetStatus {

    override def toString: String = "Ожидает экспирации"
  }
  case object BetStatusSuccess extends BetStatus {
    override def toString: String = "Выигрыш"
  }
  case object BetStatusFail extends BetStatus {
    override def toString: String = "Проигрыш"
  }
  case object BetStatusRejected extends BetStatus {
    override def toString: String = "Отмена"
  }

  override val values: IndexedSeq[BetStatus] = findValues
}
