package game.modules.bet.models

import enumeratum.{CirceEnum, Enum, EnumEntry}
import scala.collection.immutable.IndexedSeq

sealed trait BetMove extends EnumEntry {
  val value: Int
}

object BetMove extends Enum[BetMove] with CirceEnum[BetMove] {

  def unapply(move: Int): Option[BetMove] = findValues.find(_.value == move)

  case object MoveUp extends BetMove {
    override val value: Int = 1
  }

  case object MoveDown extends BetMove {
    override val value: Int = -1
  }

  override val values: IndexedSeq[BetMove] = findValues

}


