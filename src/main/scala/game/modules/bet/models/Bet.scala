package game.modules.bet.models

import org.joda.time.DateTime

case class Bet(id: Long,
               userId: Long,
               figi: Option[String],
               nameStock: Option[String],
               currency: Option[String],
               tsExpiration: Option[Long],
               tsCreate: Option[Long],
               sum: Option[Int],
               move: Option[BetMove],
               openPrice: Option[Double],
               closePrice: Option[Double],
               status: BetStatus) {
  override def toString: String = {
    val currncy = currency.getOrElse("")

    s"Заявка id: $id: \r\n" +
      nameStock.collect { case fg => s"актив: $fg \r\n"}.getOrElse("") +
      tsCreate.collect { case ts => s"дата создания: ${new DateTime(ts).toString("dd.MM.yyyy HH:mm:ss")} \r\n" }.getOrElse("") +
      tsExpiration.collect { case ts => s"дата экспирации: ${new DateTime(ts).toString("dd.MM.yyyy HH:mm:ss")} \r\n" }.getOrElse("") +
      sum.collect { case sm => s"сумма: $sm \r\n"}.getOrElse("") +
      move.collect {
        case BetMove.MoveUp => s"куда: up \r\n"
        case _ => s"куда: down \r\n"
      }.getOrElse("") +
      openPrice.collect { case sm => s"сумма на время создания: $sm $currncy\r\n" }.getOrElse("") +
      closePrice.collect { case sm => s"сумма на время исполнения: $sm $currncy\r\n" }.getOrElse("") +
      "статус: " + status.toString + "\r\n"
  }

  def isCompleted: Boolean =
    figi.isDefined && tsExpiration.isDefined && sum.isDefined && move.isDefined
}
