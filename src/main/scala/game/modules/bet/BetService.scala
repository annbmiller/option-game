package game.modules.bet

import game.modules.bet.models.{Bet, BetMove, BetStatus}
import game.modules.user.User
import org.joda.time.DateTime

import scala.concurrent.{ExecutionContext, Future}

class BetService(betRepository: BetRepository)(implicit ec: ExecutionContext) {
  private def checkStatus(bet: Bet): Bet =
    if (BetStatus.BetStatusInProgress == bet.status && bet.isCompleted)
      bet.copy(status = BetStatus.BetStatusWait, tsCreate = Some(DateTime.now.withSecondOfMinute(0).getMillis))
    else bet

  def create(user: User): Future[Bet] =
    betRepository.add(Bet(0, user.id, None, None, None, None, None, None, None, None, None, BetStatus.BetStatusInProgress))

  def find(id: Long): Future[Option[Bet]] = betRepository.find(id)

  def update(bet: Bet): Future[Bet] = betRepository.update(bet)

  def setMove(bet: Bet, move: Int): Future[Bet] = {
    val newBet = bet.copy(move = BetMove.unapply(move))
    betRepository.update(checkStatus(newBet))
  }

  def setSum(bet: Bet, sum: Int): Future[Bet] = {
    val newBet = bet.copy(sum = Some(sum))
    betRepository.update(checkStatus(newBet))
  }

  def setStatus(bet: Bet, newStatus: BetStatus): Future[Bet] = betRepository.update(bet.copy(status = newStatus))

  def setTs(bet: Bet, ts: DateTime): Future[Bet] = {
    val newBet = bet.copy(tsExpiration = Some(ts.getMillis))
    betRepository.update(checkStatus(newBet))
  }

  def setStock(bet: Bet, figi: String, currency: String, name: String): Future[Bet] = {
    val newBet = bet.copy(figi = Some(figi), currency = Some(currency), nameStock = Some(name))
    betRepository.update(checkStatus(newBet))
  }

  def getCurrentBet(user: User): Future[Bet] = betRepository.findNonCompleteFor(user.id).flatMap {
    case Some(bet) => Future.successful(bet)
    case _ => create(user)
  }

  def getBetsForCalc: Future[Seq[Bet]] = betRepository.findWaitingBeforeDate(DateTime.now.getMillis)

  def getAll: Future[Seq[Bet]] = betRepository.findAll
}
