package game.modules.bet

import slick.dbio.{DBIOAction, Effect}
import slick.jdbc.H2Profile.api._
import slick.lifted.{ForeignKeyQuery, ProvenShape}
import enumeratum.SlickEnumSupport
import game.modules.bet.models.{Bet, BetMove, BetStatus}
import game.modules.user.{User, UserTableRepository, UsersTable}
import BetTableRepository.AllBets
import slick.ast.BaseTypedType
import slick.jdbc.JdbcType

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

private[bet] class BetRepository(db: Database)(implicit ec: ExecutionContext) {
  Await.result(db.run(AllBets.schema.createIfNotExists), Duration.Inf)

  def add(bet: Bet): Future[Bet] = db.run(BetTableRepository.add(bet))

  def find(id: Long): Future[Option[Bet]] = db.run(BetTableRepository.find(id))

  def update(bet: Bet): Future[Bet] = db.run(BetTableRepository.update(bet)).map(_ => bet)

  def findNonCompleteFor(userId: Long): Future[Option[Bet]] = db.run(BetTableRepository.findNonCompleteFor(userId))

  def findWaitingBeforeDate(ts: Long): Future[Seq[Bet]] = db.run(BetTableRepository.findWaitingBeforeDate(ts))

  def findAll: Future[Seq[Bet]] = db.run(AllBets.result)
}

private object BetTableRepository extends SlickEnumSupport {
  val profile = slick.jdbc.H2Profile
  val AllBets = TableQuery[BetsTable]
  val insertQuery = AllBets returning AllBets.map(_.id) into ((item, id) => item.copy(id = id))

  implicit lazy val statusMapperBetStatus: JdbcType[BetStatus] with BaseTypedType[BetStatus] =
    mappedColumnTypeForEnum(BetStatus)

  implicit lazy val statusMapperBetMove: JdbcType[BetMove] with BaseTypedType[BetMove] =
    mappedColumnTypeForEnum(BetMove)

  def add(bet: Bet): DBIOAction[Bet, NoStream, Effect.Write] = insertQuery += bet

  def find(id: Long): DBIOAction[Option[Bet], NoStream, Effect.Read] = AllBets.filter(_.id === id).result.headOption

  def findNonCompleteFor(userId: Long): DBIOAction[Option[Bet], NoStream, Effect.Read] =
    AllBets.filter(
      bet => bet.userId === userId && bet.status === (BetStatus.BetStatusInProgress: BetStatus)
    ).result.headOption

  def findWaitingBeforeDate(ts: Long): DBIOAction[Seq[Bet], NoStream, Effect.Read] =
    AllBets.filter(bet =>
      bet.tsExpiration.isDefined &&
        bet.tsExpiration <= ts
        && bet.status === (BetStatus.BetStatusWait: BetStatus)
    ).result

  def update(bet: Bet): DBIOAction[Int, NoStream, Effect.Write] = AllBets.filter(_.id === bet.id).update(bet)

  private[bet] class BetsTable(tag: Tag) extends Table[Bet](tag, "BETS") {
    def id: Rep[Long] = column("id", O.PrimaryKey, O.AutoInc)

    def userId: Rep[Long] = column("userId")

    def figi: Rep[Option[String]] = column("figi")

    def nameStock: Rep[Option[String]] = column("nameStock")

    def currency: Rep[Option[String]] = column("currency")

    def tsExpiration: Rep[Option[Long]] = column("tsExpiration")

    def tsCreate: Rep[Option[Long]] = column("tsCreate")

    def sum: Rep[Option[Int]] = column("sum")

    def move: Rep[Option[BetMove]] = column("moveInt")

    def openPrice: Rep[Option[Double]] = column("openPrice")

    def closePrice: Rep[Option[Double]] = column("closePrice")

    def status: Rep[BetStatus] = column("status")

    def user: ForeignKeyQuery[UsersTable, User] =
      foreignKey("USER_FK", userId, UserTableRepository.AllUsers)(_.id)

    override def * : ProvenShape[Bet] =
      (id, userId, figi, nameStock, currency, tsExpiration, tsCreate, sum, move, openPrice, closePrice, status).mapTo[Bet]
  }
}
