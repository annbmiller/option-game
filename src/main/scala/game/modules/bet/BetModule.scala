package game.modules.bet

import game.db.Database

import scala.concurrent.ExecutionContext

class BetModule(db: Database)(implicit ec: ExecutionContext) extends BetModuleInterface {
  private val betRepository = new BetRepository(db.db)
  val betService            = new BetService(betRepository)
}
