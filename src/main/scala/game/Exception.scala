package game

sealed abstract class ApiException(message: String) extends Exception(message)

final case class CandlesNotFoundException(figi: String)
  extends ApiException(s"Для figi=$figi не получены свечи с биржи")

final case class InstrumentNotFoundException(figi: String)
  extends ApiException(s"Для figi=$figi не найден актив")

sealed abstract class DaemonException(message: String) extends Exception(message)

final case class BetIsNotCompleted(id: Long)
  extends DaemonException(s"Заявка $id не закончена и не может быть рассчитана.")

final case class BetHasWrongDates(id: Long)
  extends DaemonException(s"Заявка $id не может быть рассчитана. Торги по активу не велись на дату заявки или экспирации.")

sealed abstract class DbException(message: String) extends Exception(message)

final case class UserNotFoundException(id: Long)
  extends DbException(s"Пользователь с id $id не найден")
