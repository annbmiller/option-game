package game.bot

import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.api.declarative.{Callbacks, Commands}
import com.bot4s.telegram.clients.ScalajHttpClient
import com.bot4s.telegram.future.Polling
import game.modules.app.AppModule
import org.joda.time.DateTimeZone
import slogging.{LogLevel, LoggerConfig, PrintLoggerFactory}

import scala.concurrent.{ExecutionContext, Future}

class GameBot(token: String, override val app: AppModule)
             (implicit val executionContext: ExecutionContext) extends BotLogic
  with Polling
  with Commands[Future]
  with Callbacks[Future] {

  LoggerConfig.factory = PrintLoggerFactory()
  LoggerConfig.level   = LogLevel.TRACE
  DateTimeZone.setDefault(DateTimeZone.forOffsetHours(3))

  override val client: RequestHandler[Future] = new ScalajHttpClient(token)

  onCommand("start") { msg => startMessage(msg) }

  onCommand("balance") { msg => balanceMessageCallback(msg) }

  onCommand("top") { msg => topMessage(msg) }

  onCommand("date") { msg => dateChoiceMessage(msg) }

  onCommand("shares") { msg => instrumentChoiceMessage(msg) }

  onCallbackWithTag(TAG_BALANCE) { implicit cbq => replyCallback(balanceMessageCallback) }

  onCallbackWithTag(TAG_START) { implicit cbq => replyCallback(startMessage) }

  onCallbackWithTag(TAG_TOP) { implicit cbq => replyCallback(topMessage) }

  onCallbackWithTag(TAG_START_BET) { implicit cbq => replyCallback(instrumentChoiceMessage) }

  onCallbackWithTag(TAG_INSTRUMENT) { implicit cbq => setFigi }

  onCallbackWithTag(TAG_DATE) { implicit cbq => setDate }

  onCallbackWithTag(TAG_MOVE) { implicit cbq => setMove }

  onCallbackWithTag(TAG_SUM) { implicit cbq => setSum }
}