package game.bot

import cats.MonadError
import cats.instances.future._
import cats.syntax.functor._
import com.bot4s.telegram.Implicits._
import com.bot4s.telegram.api.BotBase
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.models._

import game.api.market.Stocks
import game.modules.bet.BetService
import game.modules.bet.models.{Bet, BetMove, BetStatus}
import game.modules.user.{User, UserService}
import game.modules.app.AppModule

import com.github.nscala_time.time.Imports._
import game.NotificationInterface
import slogging.StrictLogging
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

trait BotLogic extends BotBase[Future]
  with StrictLogging
  with NotificationInterface {

  def app: AppModule

  implicit val executionContext: ExecutionContext

  override val monad: MonadError[Future, Throwable] = MonadError[Future, Throwable]

  private val userService: UserService = app.userModule.userService
  private val betService: BetService   = app.betModule.betService
  private val stocks: Stocks           = app.stocks

  val TAG_BALANCE = "BALANCE"
  val TAG_TOP = "TOP"
  val TAG_START_BET = "BET_START"
  val TAG_START = "START"
  val TAG_DATE = "DATE"
  val TAG_INSTRUMENT = "INSTRUMENT"
  val TAG_MOVE = "MOVE"
  val TAG_SUM = "SUM"

  val minutesAfterNow = Seq(5, 10, 15)

  val sumsForBet = Seq(10, 50, 100)

  val balanceButton: InlineKeyboardButton = getButton("Баланс", TAG_BALANCE)

  val topButton: InlineKeyboardButton     = getButton("TOP 10", TAG_TOP)

  val betButton: InlineKeyboardButton     = getButton("Сделать ставку", TAG_START_BET)

  val getRegButton: InlineKeyboardButton  = getButton("Зарегистрироваться", TAG_START)

  def getDateButton(date: DateTime): InlineKeyboardButton = {
    val rDate = date.second(0)
    getButton(rDate.toString("dd.MM.yyyy HH:mm:ss"), TAG_DATE + rDate.toString)
  }

  def getButton(text: String, tag: String): InlineKeyboardButton = InlineKeyboardButton.callbackData(text, tag)

  def markupMain: InlineKeyboardMarkup = InlineKeyboardMarkup.singleRow(Seq(balanceButton, betButton, topButton))

  def markupStart: InlineKeyboardMarkup = InlineKeyboardMarkup.singleButton(getRegButton)

  def replyToChatId(id: ChatId, text: String, mark: Option[ReplyMarkup]): Future[Unit] = request(
    SendMessage(id, text, replyMarkup = mark)).void

  /**
   * Выбор следующего сообщения для формирования заявки
   * @param bet: Bet
   * @param msg: Message
   */
  private def getNextMessageForComplete(bet: Bet)(msg: Message): Future[Unit] = if (bet.status == BetStatus.BetStatusInProgress) bet match {
    case bet if bet.figi.isEmpty => instrumentChoiceMessage(msg)
    case bet if bet.tsExpiration.isEmpty => dateChoiceMessage(msg)
    case bet if bet.move.isEmpty => moveChoiceMessage(msg)
    case bet if bet.sum.isEmpty => sumChoiceMessage(msg)
  } else completeMessage(bet)

  /**
   * Сообщение о том, что заявка укомплектована
   * @param bet: Bet
   */
  def completeMessage(bet: Bet): Future[Unit] =
    replyToChatId(bet.userId, "Заявка собрана!\r\n" + bet.toString, None)

  /**
   * Реализация NotificationInterface
   */

  override def sendErrorCalculationMessage: (Long, String) => Future[Unit] =
    (id: Long, error: String) => {
      logger.error(error)
      replyToChatId(id, error, markupMain)
    }

  override def sendErrorTimingMessage: Long => Future[Unit] = (id: Long) =>
    replyToChatId(
      id,
      "К сожалению, на дату создания или расчета заявки, торги не проводились. \r\n Сделайте еще одну: ",
      markupMain
    )

  override def sendEndOfCalcMessage: Bet => Future[Unit] = (bet: Bet) => {
    val message = s"Заявка исполнена: \r\n" + bet.toString
    for {
      _ <- replyToChatId(bet.userId, message, markupMain)
      _ <- balanceMessage(bet.userId)
    } yield ()
  }

  /**
   * Методы сообщений
   */

  def sendUserInfoMain(id: ChatId)(usr: User): Future[Unit] = replyToChatId(
    id,
    s"Привет, ${usr.name}, твой баланс ${usr.score}",
    markupMain
  )

  def startMessage(msg: Message): Future[Unit] = userService.findOrAdd(msg.chat).flatMap(sendUserInfoMain(msg.chat.id))

  def topMessage(msg: Message): Future[Unit] = userService.getTop.flatMap { users =>
    replyToChatId(
      msg.chat.id,
      users.foldLeft("")((ac, user) => ac + s"${user.name} - ${user.score} \r\n"),
      None
    ).void
  }

  def balanceMessage(userId: Long): Future[Unit] = userService.find(userId).map {
    case Some(usr) => sendUserInfoMain(userId)(usr)
    case _ => replyToChatId(userId, "Для начала нужно зарегистрироваться :)", markupStart)
  }

  def balanceMessageCallback(msg: Message): Future[Unit] = balanceMessage(msg.chat.id)

  def dateChoiceMessage(msg: Message): Future[Unit] = dateChoiceTextMessage("Выберите дату экспирации")(msg)

  def dateChoiceTextMessage(text: String)(msg: Message): Future[Unit] = {
    val now = DateTime.now
    replyToChatId(msg.chat.id, text,
      InlineKeyboardMarkup.singleColumn(minutesAfterNow.map(min => getDateButton(now.plus(min * 60000))))
    )
  }

  def moveChoiceMessage(msg: Message): Future[Unit] = moveChoiceTextMessage("Выберите движение цены")(msg)

  def moveChoiceTextMessage(text: String)(msg: Message): Future[Unit] = replyToChatId(msg.chat.id, text,
    InlineKeyboardMarkup.singleRow(Seq(
      getButton("Вверх", TAG_MOVE + BetMove.MoveUp.value),
      getButton("Вниз", TAG_MOVE + BetMove.MoveDown.value)
    ))
  )

  def sumChoiceMessage(msg: Message): Future[Unit] = sumChoiceTextMessage("Выберите сумму очков")(msg)

  def sumChoiceTextMessage(text: String)(msg: Message): Future[Unit] = replyToChatId(msg.chat.id, text,
    InlineKeyboardMarkup.singleColumn(sumsForBet.map(sum => getButton(sum.toString, TAG_SUM + sum)))
  )

  def instrumentChoiceTextMessage(text: String)(msg: Message): Future[Unit] = stocks.getTop.map { instruments =>
    replyToChatId(
      msg.chat.id,
      text,
      InlineKeyboardMarkup.singleColumn(
        instruments.map(instr => getButton(s"${instr.ticker} - ${instr.name}", TAG_INSTRUMENT + instr.figi))
      )
    )
  }

  def instrumentChoiceMessage(msg: Message): Future[Unit] = instrumentChoiceTextMessage("Выберите актив")(msg)


  /**
   *  Callbacks после нажатий кнопок
   */

  def replyCallback(f: Message => Future[Unit])(implicit cbq: CallbackQuery): Future[Unit] = cbq.message match {
    case Some(message: Message) => f(message)
    case _ => Future.successful(())
  }

  def setFigi(implicit cbq: CallbackQuery): Future[Unit] = {
    val data = for {
      figi <- cbq.data
      msg  <- cbq.message
    } yield (figi, msg)

    val result = data match {
      case Some(data) => for {
        instrmnt <- stocks.find(data._1)
        if instrmnt.isDefined
        usr <- userService.findOrAdd(data._2.chat)
        instrument = instrmnt.get
        bet <- betService.getCurrentBet(usr).flatMap(
          betService.setStock(_, data._1, instrument.currency, instrument.name)
        )
        response <- getNextMessageForComplete(bet)(data._2)
      } yield response
      case _ => Future.successful(())
    }

    result.recover { case e =>
      logger.error(e.getMessage)
      replyCallback(instrumentChoiceTextMessage("Актив не найден, выберите другой"))
    }
  }

  def setDate(implicit cbq: CallbackQuery): Future[Unit] = {
    val maybeSetDateFuture = for {
      date <- cbq.data
      datetime = new DateTime(date)
      if datetime.isAfterNow
      msg <- cbq.message
      usr <- userService.findOrAdd(msg.chat)
      bet <- betService.getCurrentBet(usr).flatMap(betService.setTs(_, datetime))
      response <- getNextMessageForComplete(bet)(msg)
    } yield response
    maybeSetDateFuture.getOrElse(replyCallback(dateChoiceTextMessage("Что-то пошло не так, выберите дату снова")))
  }

  def setMove(implicit cbq: CallbackQuery): Future[Unit] = {
    val maybeSetMoveFuture = for {
      data <- cbq.data
      Int(move) = data
      msg <- cbq.message
      usr <- userService.findOrAdd(msg.chat)
      bet <- betService.getCurrentBet(usr).flatMap(betService.setMove(_, move))
      response <- getNextMessageForComplete(bet)(msg)
    } yield response
    maybeSetMoveFuture.getOrElse(replyCallback(moveChoiceTextMessage("Что-то пошло не так, выберите снова")))
  }

  def setSum(implicit cbq: CallbackQuery): Future[Unit] = {
    val maybeSetSumFuture = for {
      data <- cbq.data
      Int(sum) = data
      msg <- cbq.message
      usr <- userService.findOrAdd(msg.chat)
      bet <- betService.getCurrentBet(usr).flatMap(betService.setSum(_, sum))
      response <- getNextMessageForComplete(bet)(msg)
    } yield response
    maybeSetSumFuture.getOrElse(replyCallback(sumChoiceTextMessage("Что-то пошло не так, выберите снова")))
  }


  object Int {
    def unapply(s: String): Option[Int] = Try(s.toInt).toOption
  }

}
