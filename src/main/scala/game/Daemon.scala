package game

import java.util.concurrent.TimeUnit
import akka.actor.{ActorSystem, Cancellable}
import scala.concurrent.duration.FiniteDuration
import slogging.StrictLogging
import scala.concurrent.{ExecutionContext, Future}

import game.api.market.Candles
import game.api.models.ListCandles
import game.modules.app.AppModule
import game.modules.bet.BetService
import game.modules.bet.models.{Bet, BetMove, BetStatus}
import game.modules.user.UserService

/**
 * Демон для расчета заявок
 */
class Daemon(app: AppModule, sendService: NotificationInterface)
            (implicit system: ActorSystem, ec: ExecutionContext) extends StrictLogging {

  private val timeout                  = new FiniteDuration(1, TimeUnit.MINUTES)
  private val userService: UserService = app.userModule.userService
  private val betService: BetService   = app.betModule.betService
  private val candles: Candles         = app.candles

  @volatile private var scheduler: Cancellable = Cancellable.alreadyCancelled

  /**
   * Расчет заявки
   * @param bet: Bet
   * @param openList: ListCandles
   * @param closeList: ListCandles
   */
  private def completeBet(bet: Bet, openList: ListCandles, closeList: ListCandles): Future[Unit] = {
    if (!bet.isCompleted) {
      Future.failed(BetIsNotCompleted(bet.id))
    } else if (openList.candles.isEmpty || closeList.candles.isEmpty) {
      Future.failed(BetHasWrongDates(bet.id))
    } else {
      bet match {
        case Bet(_, _, _, _, _, _, _, Some(sum), Some(move), _, _, _) =>
          val open  = openList.candles.head.c
          val close = closeList.candles.head.c

          val newStatus = move match {
            case BetMove.MoveUp   if close >= open => BetStatus.BetStatusSuccess
            case BetMove.MoveDown if close < open  => BetStatus.BetStatusSuccess
            case _ => BetStatus.BetStatusFail
          }

          val completedBet = bet.copy(status = newStatus, openPrice = Some(open), closePrice = Some(close))

          val newScore = newStatus match {
            case BetStatus.BetStatusSuccess => (score: Int) => score + sum
            case BetStatus.BetStatusFail    => (score: Int) => score - sum
            case _ => (score: Int) => score
          }

          for {
            _ <- betService.update(completedBet)
            _ <- userService.findAndSetScore(completedBet.userId, newScore)
            _ <- sendService.sendEndOfCalcMessage(completedBet)
          } yield ()

        case _ => Future.failed(BetIsNotCompleted(bet.id))
      }
    }
  }

  /**
   * Запрашивает свечи и вызывает completeBet для расчета
   * @param bet: Bet
   */
  private def calcBet(bet: Bet): Future[Unit] = {
    val dates = for {
      openDate  <- bet.tsCreate
      closeDate <- bet.tsExpiration
      figi      <- bet.figi
    } yield (figi, openDate, closeDate)

    dates match {
      case Some(data) =>
        for {
          open   <- candles.getForTime(data._1, data._2)
          close  <- candles.getForTime(data._1, data._3)
          result <- completeBet(bet, open, close)
        } yield result
      case _ =>
        Future.failed(BetIsNotCompleted(bet.id))
    }

  }

  /**
   * Запрашивает все заяввки у которых подошла дата экспирации
   */
  private def runJob: Future[Unit] =
    betService.getBetsForCalc.flatMap(list => Future.sequence(
      list.map(bet =>
        calcBet(bet).recover {
          case _: CandlesNotFoundException =>
            betService.update(bet.copy(status = BetStatus.BetStatusRejected)) zip sendService.sendErrorTimingMessage(bet.userId)
          case e =>
            betService.update(bet.copy(status = BetStatus.BetStatusRejected)) zip sendService.sendErrorCalculationMessage(bet.userId, e.getMessage)
        })
    ).map(_ => ()))

  def runDaemon(): Unit = synchronized {
    scheduler = system.scheduler.scheduleWithFixedDelay(timeout, timeout) { () => runJob }
  }

  def shutdownDaemon(): Unit = synchronized {
    if (scheduler.isCancelled) {
      throw new RuntimeException("Daemon is not running")
    }
    scheduler.cancel()
  }
}
