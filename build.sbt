name := "OptionGame"

version := "0.1"

scalaVersion := "2.12.12"
mainClass in (Compile, run) := Some("game.Main")
cancelable in Global := true

libraryDependencies ++= Seq(
  "com.beachape" %% "enumeratum" % "1.5.15",
  "com.beachape" %% "enumeratum-circe" % "1.5.15",
  "com.beachape" %% "enumeratum-slick" % "1.5.15",

  "com.github.nscala-time" %% "nscala-time" % "2.26.0",
  "com.bot4s" %% "telegram-core" % "4.4.0-RC2",

  "com.typesafe.akka" %% "akka-actor" % "2.6.10",
  "com.typesafe.akka" %% "akka-stream" % "2.6.10",
  "com.typesafe.akka" %% "akka-http" % "10.2.1",

  "com.h2database" % "h2" % "1.4.200",
  "com.typesafe.slick" %% "slick" % "3.3.3",
  "org.slf4j" % "slf4j-nop" % "1.6.4",

  "org.scalatest" %% "scalatest" % "3.2.0" % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.10" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % "10.2.1" % Test,
)
